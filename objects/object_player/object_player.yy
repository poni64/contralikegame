{
    "id": "dd7bb72d-3518-484f-8e93-b778e410fb28",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_player",
    "eventList": [
        {
            "id": "406f21a2-55a0-45c2-accc-986982df5295",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 5,
            "m_owner": "dd7bb72d-3518-484f-8e93-b778e410fb28"
        },
        {
            "id": "a1944183-cc45-43e1-9ae5-928beccc19a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 5,
            "m_owner": "dd7bb72d-3518-484f-8e93-b778e410fb28"
        },
        {
            "id": "363ee31c-4951-42bb-8d8b-64aad18f704c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 5,
            "m_owner": "dd7bb72d-3518-484f-8e93-b778e410fb28"
        },
        {
            "id": "74d04e62-0f0d-4836-bad6-46e0a9310595",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "dd7bb72d-3518-484f-8e93-b778e410fb28"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cdbbd996-2663-46a0-9cb0-79cb5455b2fa",
    "visible": true
}