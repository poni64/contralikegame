{
    "id": "cdbbd996-2663-46a0-9cb0-79cb5455b2fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 11,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65cd2748-c110-48af-b9d7-fbc5be642e8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cdbbd996-2663-46a0-9cb0-79cb5455b2fa",
            "compositeImage": {
                "id": "4c5b1b79-396b-4a39-9417-7ddd87fc0efb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65cd2748-c110-48af-b9d7-fbc5be642e8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "004cd983-76ae-4f57-a66d-5d5b945a84bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65cd2748-c110-48af-b9d7-fbc5be642e8a",
                    "LayerId": "c4b428c3-ff6d-46d6-b5c8-3ce270579478"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 32,
    "layers": [
        {
            "id": "c4b428c3-ff6d-46d6-b5c8-3ce270579478",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cdbbd996-2663-46a0-9cb0-79cb5455b2fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}