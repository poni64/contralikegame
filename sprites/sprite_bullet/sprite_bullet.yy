{
    "id": "7ef634e4-516d-4252-a9ec-19d46a6a83a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f2c5c01-95ea-455e-a45f-39befe41565f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ef634e4-516d-4252-a9ec-19d46a6a83a8",
            "compositeImage": {
                "id": "d81716f9-a021-4d08-803a-b0a5951ba52d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f2c5c01-95ea-455e-a45f-39befe41565f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d2e86e4-bb9e-42f8-98ce-fee875885634",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f2c5c01-95ea-455e-a45f-39befe41565f",
                    "LayerId": "6cc7a869-8b32-4021-90df-02c2b5fe02aa"
                }
            ]
        }
    ],
    "gridX": 1,
    "gridY": 1,
    "height": 16,
    "layers": [
        {
            "id": "6cc7a869-8b32-4021-90df-02c2b5fe02aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ef634e4-516d-4252-a9ec-19d46a6a83a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}